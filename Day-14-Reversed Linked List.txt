//EASY 
//https://leetcode.com/problems/reverse-linked-list/
//Reverse Linked List

/**
 * Example:
 * var li = ListNode(5)
 * var v = li.`val`
 * Definition for singly-linked list.
 * class ListNode(var `val`: Int) {
 *     var next: ListNode? = null
 * }
 */
class Solution {
    fun reverseList(head: ListNode?): ListNode? {
        return reverse(head, null)   
    }
        
        fun reverse(currentNode:ListNode?, previousNode:ListNode?):ListNode?{
            if(currentNode?.next == null){
                currentNode?.next = previousNode
                return currentNode
            }

            val headNode = reverse(currentNode?.next, currentNode)
            currentNode?.next = previousNode
            return headNode
            
        }
}